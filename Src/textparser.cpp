#include "textparser.h"

textParser::textParser(QObject *parent) :
    QObject(parent),
    m_inputFilePath(""),
    m_outputFilePath(""),
    m_fileContent(nullptr)
{

}

/***************************
 *
 * Public methods
 *
 * ************************/
int textParser::setInputFilePath(QString inputFilePath)
{
    int result = NO_ERROR;
    QFile inputFile(inputFilePath);
    qDebug()<<"input path: " << inputFilePath;
    if(inputFile.exists())
    {
        m_inputFilePath = inputFilePath;
    }
    else
    {
        qDebug()<<"Error the input file does not exist. Cannot sort file content.";
        result = ERROR_FILE_NOT_FOUND;
    }
    return result;
}

void textParser::setOutputFilePath(QString outputFilePath)
{
    QFile outputFile(outputFilePath);
    if(outputFile.exists())
    {
        qDebug()<<"Warning output file already exists, will overwrite it.";
    }
    m_outputFilePath = outputFilePath;
}

int textParser::sortFilecontent(bool sortAlphabetical, bool reverseOrder, bool removeDuplicate)
{
    int result = getInputFileContent();
    if(NO_ERROR == result && removeDuplicate)
    {
        int numberOfduplicate = m_fileContent.removeDuplicates();
        qDebug()<< numberOfduplicate <<" words have been removed as they were duplicates";
    }
    if( NO_ERROR == result && sortAlphabetical)
    {
        m_fileContent.sort(Qt::CaseInsensitive);
        if(reverseOrder)
        {
            std::reverse(m_fileContent.begin(),m_fileContent.end());
        }
    }
    if (NO_ERROR == result)
    {
        result = writeOutputFile();
    }
    return result;
}

/***************************
 *
 * Private methods
 *
 * ************************/
int textParser::getInputFileContent()
{
    int result = NO_ERROR;
    QFile inputFile(m_inputFilePath);
    if(inputFile.open(QFile::ReadOnly | QFile::Text))
    {
        QTextStream textStream( &inputFile );
        m_fileContent = textStream.readAll().split( "\n" );
        inputFile.close();
        if(m_fileContent.isEmpty())
        {
            qDebug()<<"Error empty file for input, cannot sort it";
            result = ERROR_FILE_EMPTY;
        }
    }
    else
    {
        qDebug()<<"Error cannot open input file.";
        result = ERROR_CANNOT_OPEN_FILE;
    }
    return result;
}

int textParser::writeOutputFile()
{
    int result = NO_ERROR;
    QFile outputFile(m_outputFilePath);
    if(outputFile.open(QFile::WriteOnly|QFile::Text))
    {
        QTextStream textSet( &outputFile );
        outputFile.resize( 0 );
        textSet << m_fileContent.join("\n");
        outputFile.flush();
        outputFile.close();
        m_fileContent.clear();
    }
    else
    {
        qDebug()<<"Error cannot open output file.";
        result = ERROR_CANNOT_OPEN_FILE;
    }
    return result;
}
