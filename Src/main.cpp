#include <QCoreApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include "textparser.h"

struct CommandParameters
{
    bool sort;
    bool reverseOrder;
    bool removeDuplicate;
    QString inputFilePath;
    QString outputFilePath;
};

void setCommandLineArguments( QCommandLineParser&  parser )
{
    parser.addPositionalArgument( "inputFilePath", QCoreApplication::translate( "main", "Path to file containing list to be sorted. Should be text file with one word per line.\nMandatory." ) );
    parser.addPositionalArgument( "outputFilePath", QCoreApplication::translate( "main", "Path to output file. Will be text file with one word per line.\nIf empty, will overwrite inputFile." ) );
}

void setCommandLineOptions( QCommandLineParser& parser )
{
    QCommandLineOption sort( QStringList() << "s" << "sort", QCoreApplication::translate( "main", "sort alphabetically the list (case insensitive)" ) );
    QCommandLineOption reverseOrder( QStringList() << "R" << "reverse", QCoreApplication::translate( "main", "sort in reverse alphabetical the list (case insensitive)" ) );
    QCommandLineOption removeDuplicate( QStringList() << "r" << "remove", QCoreApplication::translate( "main", "remove duplicates in list" ) );

    parser.addOption( sort );
    parser.addOption( reverseOrder );
    parser.addOption( removeDuplicate );

}

bool parseCommandLine( QCoreApplication& app, CommandParameters& parameters )
{
    QCommandLineParser parser;

    parser.setApplicationDescription( QCoreApplication::translate( "main", "SortWordList Application sorts a file list of words in a file." ) );
    parser.addHelpOption();
    parser.addVersionOption();
    setCommandLineArguments( parser );
    setCommandLineOptions( parser );

    parser.process( app );

    bool shouldParseFile = false;

    if(parser.isSet("help"))
    {
        parser.showHelp();
    }
    else if ( parser.positionalArguments().size() == 0 )
    {
        fprintf( stderr, "%s\n", qPrintable( QCoreApplication::translate( "main", "Input file path is mandatory.\n" ) ) );
        parser.showHelp();
    }
    else
    {
        shouldParseFile = true;
        parameters.inputFilePath = parser.positionalArguments().at( 0 );
        if( parser.positionalArguments().size() > 1 )
        {
            parameters.outputFilePath = parser.positionalArguments().at( 1 );
        }
        if ( parser.isSet( "sort" ) )
        {
            parameters.sort = true;
        }
        else
        {
            parameters.sort = false;
        }
        if ( parser.isSet( "reverse" ) )
        {
            parameters.sort = true;
            parameters.reverseOrder = true;
        }
        else
        {
            parameters.reverseOrder = false;
        }
        if( parser.isSet( "remove" ) )
        {
            parameters.removeDuplicate = true;
        }
        else
        {
            parameters.removeDuplicate = false;
        }
    }
    return shouldParseFile;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QCoreApplication::setApplicationName( "SortWordList" );
    QCoreApplication::setApplicationVersion( "1.0" );

    CommandParameters parameters;

    if(parseCommandLine( a,  parameters ))
    {
        textParser fileParser;
        if( NO_ERROR == fileParser.setInputFilePath(parameters.inputFilePath) )
        {
            if(parameters.outputFilePath.isEmpty())
            {
                fileParser.setOutputFilePath(parameters.inputFilePath);
            }
            else
            {
                fileParser.setOutputFilePath(parameters.outputFilePath);
            }
            fileParser.sortFilecontent(parameters.sort, parameters.reverseOrder, parameters.removeDuplicate);
            fprintf( stdout, "%s\n", qPrintable( QCoreApplication::translate( "main", "Done." ) ) );
        }
    }
    else
    {
        fprintf( stderr, "%s\n", qPrintable( QCoreApplication::translate( "main", "Cannot parse command, please retry." ) ) );
    }

    return 0;
}
