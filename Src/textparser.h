#ifndef TEXTPARSER_H
#define TEXTPARSER_H

#include <QObject>
#include <QStringList>
#include <QFile>
#include <QDebug>

#define NO_ERROR 0
#define ERROR_FILE_NOT_FOUND -1
#define ERROR_CANNOT_OPEN_FILE -2
#define ERROR_FILE_EMPTY    -3
class textParser : public QObject
{
    Q_OBJECT
public:
    explicit textParser(QObject *parent = nullptr);
    int setInputFilePath(QString inputFilePath);
    void setOutputFilePath(QString outputFilePath);
    int sortFilecontent(bool sortAlphabetical, bool reverseOrder, bool removeDuplicate);

private:
    QString m_inputFilePath;
    QString m_outputFilePath;
    QStringList m_fileContent;

    int getInputFileContent();
    int writeOutputFile();


signals:

public slots:
};

#endif // TEXTPARSER_H
