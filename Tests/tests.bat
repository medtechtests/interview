::Check std cases
..\Bin\SortWordList.exe -r input\file1_simpleTests.txt output\file1_removed.txt
..\Bin\SortWordList.exe --reverse input\file1_simpleTests.txt output\file1_reverseSorted.txt
..\Bin\SortWordList.exe -s -R -r input\file1_simpleTests.txt output\file1_reverseSorted_removed.txt
..\Bin\SortWordList.exe -s .input\file1_simpleTests.txt output\file1_sorted.txt
..\Bin\SortWordList.exe --sort --remove input\file1_simpleTests.txt output\file1_sorted_removed.txt
..\Bin\SortWordList.exe input\file1_simpleTests.txt output\file1_unchanged.txt
..\Bin\SortWordList.exe --sort --reverse --remove input\file2_longList.txt output\file2_reverseSorted_removed.txt
..\Bin\SortWordList.exe -s -r input\file2_longList.txt output\file2_sorted_removed.txt
..\Bin\SortWordList.exe -h
..\Bin\SortWordList.exe -v
::Check error cases
..\Bin\SortWordList.exe 
..\Bin\SortWordList.exe unknownFilePath.txt

pause