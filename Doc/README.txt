Usage: SortWordList.exe [options] inputFilePath outputFilePath

SortWordList Application sorts a file list of words in a file.

Options:
  -?, -h, --help  Displays this help.
  -v, --version   Displays version information.
  -s, --sort      sort alphabetically the list (case insensitive)
  -R, --reverse   sort in reverse alphabetical the list (case insensitive)
  -r, --remove    remove duplicates in list

Arguments:
  inputFilePath   Path to file containing list to be sorted. Should be text
                  file with one word per line. 
				  Mandatory.
  outputFilePath  Path to output file. Will be text file with one word per
                  line. 
				  If empty, will overwrite inputFile.
				  
				  
Example:
SortWordList.exe -s C:\SOFT\interview\tests\input\file1_simpleTests.txt C:\SOFT\interview\tests\output1.txt